﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class square{

	private string imagePath;
	private string audioPath;

	public square(string image, string audio){
		imagePath = image;
		audioPath = audio;
	}

	public string ImagePath{
		get{
			return imagePath;
		}set{
			imagePath = value;
		}
	}
	public string AudioPath{
		get{
			return audioPath;
		}set{
			audioPath = value;
		}
	}


}
