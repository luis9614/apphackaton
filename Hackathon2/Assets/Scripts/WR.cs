﻿using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public static class WR {
	static private ArrayList list= new ArrayList();

	  
	public static void Read(string path){
		string line1, line2;
		try{
			StreamReader sr=new StreamReader(path,true);
			line1=sr.ReadLine();
			line2=sr.ReadLine();

			while(line1!=null && line2!= null){
				square r= new square(line1, line2);
				list.Add(r);
				line1=sr.ReadLine();
				line2=sr.ReadLine();

			}
			sr.Close();
		}catch(Exception e){
			Debug.Log ("Exception " + e.Message);
		}

	}

	public static void Write(string image, string audio, string path){
		try{
			StreamWriter sw = new StreamWriter(path,true);
			sw.Write(image);
			sw.Write(audio);
			sw.Close();
		}catch(Exception e){
			Debug.Log ("Exception " + e.Message);
		}
	}

	public static ArrayList getAndEraseList(){
		ArrayList aux = list;
		list.Clear ();
		//list.Clear;
		return aux;
	}

	public static ArrayList getList(){
		return list;
	}
}
